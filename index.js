// 1.


db.users.find({
        $or: [
        {firstName:{$regex: 's',}},
        {firstName:{$regex: 'S',}},
        {lastName: {$regex: 'd',}},
        {lastName: {$regex: 'D',}}


        ]
    });



// 2.


db.users.find({
    $and: [
        {department: "HR"},
        {age: {$gte:70}} 
      
         ]
    }
);



// 3.


db.users.find({
    $and: [
         {firstName:{$regex: 'e',}},
        {age: {$lte:30}} 
      
         ]
    }
);

